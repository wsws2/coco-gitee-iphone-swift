//
//  LoginViewModel.swift
//  Git@OSC
//
//  Created by strayRed on 2019/5/9.
//  Copyright © 2019 Git@OSC. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa



final class LoginViewModel: BaseViewMoelType {
    
    init() {}
    
    var navigationTitle: Driver<String?> { return Driver.just(String.Local.login) }
    
    lazy var request: Observable<User> = { return self.actionRequestWith(api: .none)! }()
    
    func actionRequestWith<T>(api: ActionAPI) -> Observable<T>? {
        let source =  createActionRequest(with: api, creation: { (observer: AnyObserver<User>) in
            HttpsManager.request(with: api).responseObject(completionHandler: ResponseHandler.handleObjectResponse(via: observer, success: { (object: User) in
                observer.onNext(object)
                observer.onCompleted()
                
            }))
        })
    return source as? Observable<T>
    }
}
